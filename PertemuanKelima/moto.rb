class Moto
    def initialize(name, color)
        @name = name
        @color = color
    end

    def info
        "My Motorcycle is #{@name} and color is #{@color}"
    end

    attr_accessor :name

    attr_accessor :color


end

my_moto = Moto.new("Bebek", "Red")
puts my_moto.info
my_moto.name = "GL Pro"
puts my_moto.info
my_moto.color = "hideung"
puts my_moto.info