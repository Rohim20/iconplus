class Shape
    def circumference
    end

    def area
    end
end

class Circle < Shape
    def initialize (jarijari)
        @jarijari = jarijari
    end

    def circumference
        (22 * jarijari / 7 * jarijari).round(2)
    end

    def area
        (22 * 2 * jarijari / 7).round(2)
    end

    attr_accessor :phi
    attr_accessor :jarijari
end

class Rectangle < Shape
    def initialize (panjang, lebar)
        @panjang = panjang
        @lebar = lebar
    end

    def circumference
        panjang * lebar
    end

    def area
        panjang + panjang + lebar + lebar
    end

    attr_accessor :panjang
    attr_accessor :lebar
end
puts "Selamat Datang !!!"
puts "1. Menghitung Luas dan Keliling Lingkaran"
puts "2. Menghitung Luas dan Keliling Persegi Panjang"
puts "Masukan pilihan anda : "
pilih = gets.chomp.to_i
case pilih
when 1
    puts "Menghitung Luas dan Keliling Lingkaran"
    puts "Masukan jari-jari Lingkaran : "
    jarijari = gets.chomp.to_f

    lingkaran = Circle.new(jarijari)

    puts "Luas :"
    puts lingkaran.circumference
    puts "Keliling :"
    puts lingkaran.area

when 2
    puts "Menghitung Luas dan Keliling Persegi Panjang"
    puts "Masukan Lebar Persegi Panjang"
    lebar = gets.chomp.to_f
    puts "Masukan Panjang Persegi Panjang : "
    panjang = gets.chomp.to_f

    persegi = Rectangle.new(panjang, lebar)
    puts "Luas :"
    puts persegi.circumference
    puts "Keliling :"
    puts persegi.area
end



