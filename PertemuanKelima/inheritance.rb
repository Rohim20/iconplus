class Transportation
    def initialize(max_speed)
        @max_speed = max_speed
    end
    attr_accessor :max_speed
end

module Wheel
    def wheel_count
        @wheel_count
    end
    attr_accessor :wheel_count
end

class Moto < Transportation
    include Wheel
    def initialize(name, color, max_speed=0)
        @name = name
        @color = color
        @max_speed = max_speed
        @wheel_count = 2
    end

    def info
        puts "Motor saya #{@name} warna #{@color}"
    end

    attr_accessor :name
    attr_accessor :color
end

class Ship < Transportation
    def initialize(weight, type)
        @weight = 5000
        @type = "Tonkang"
    end

    class << self
    private 
    def is_ok
        puts "It's OK"
    end
    public 
    def is_overweight
        puts "It's Overweight"
    end
    protected 
    def is_protected
        puts "It's Protected"
    end

    attr_accessor :weight
    attr_accessor :type
end

ship = Ship.new(5000, "Nanas")
puts Ship.is_overweight
