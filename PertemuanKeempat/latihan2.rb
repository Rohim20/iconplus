name = [
    "Ahmad",
    "Tono",
    "Tini",
    "Bambang",
    "Agus",
    "Agung"
]

name.each {
    |iterasi|
    if ((iterasi.length % 2 != 0) && (iterasi.slice(0) == "A"))
        puts iterasi.upcase
    elsif (iterasi.slice(0) == "T")
        puts iterasi.downcase
    else puts iterasi.reverse.capitalize
    end
}


