@data = {
  "Alice": 75,
  "Bob": 80,
  "Candra": 100,
  "Dede": 64,
  "Eka": 90
}

def average
  jumlah = 0
  @data.each do |nama, nilai|
    jumlah = jumlah + nilai
  end
  ratarata = jumlah / @data.length
  bagus = 0
  @data.each do |nama, nilai|
    if nilai > ratarata
      bagus = bagus + 1
    end
  end  
  puts "Rata-rata nilai dikelas: #{ratarata}"
  puts "Nilai yang diatas rata-rata berjumlah: #{bagus}"
end

def index
  @data.each do |nama, nilai|
	if (nilai >= 80)
		puts " Nama : #{nama}, Nilai: #{nilai}, Index: A"
	elsif ((nilai < 80) && (nilai >= 70))
		puts " Nama : #{nama}, Nilai: #{nilai}, Index: B"
	elsif ((nilai < 70) && (nilai >= 60))
		puts " Nama : #{nama}, Nilai: #{nilai}, Index: C"
	elsif ((nilai < 60) && (nilai >= 40))
		puts " Nama : #{nama}, Nilai: #{nilai}, Index: D"
	else 
		puts " Nama : #{nama}, Nilai: #{nilai}, Index: E"
	end
  end
end

average
index
